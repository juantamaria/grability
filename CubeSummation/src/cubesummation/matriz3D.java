/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cubesummation;

/**
 *
 * @author juan.santamaria
 */
public class matriz3D {
    private int[][][]matriz;
    private int[] matriz_suma; 
    private int n;

    public matriz3D(int n, int m) {
        this.matriz = new int[n][n][n];
        this.matriz_suma = new int[m+1];
        this.n = n;
    }

    public int[][][] getMatriz() {
        return matriz;
    }

    public void setMatriz(int[][][] matriz) {
        this.matriz = matriz;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void update(coordenadas c, int w){
        int x1 = 0;
        int xi = 0;
        int xf = 0;
        if(this.matriz[c.getXi()][c.getYi()][c.getZi()] == 0){
            this.matriz[c.getXi()][c.getYi()][c.getZi()] = w;
        }
        xi = generaCoordenadas(0, matriz[0].length);
        xf = generaCoordenadas(0, matriz[0].length);
        if(xi > xf){
            x1 = xf;
            xf = xi;
            xi = x1;
        }
        c.setXi(xi);
        c.setXf(xf);
        c.setYi(xi);
        c.setYf(xf);
        c.setZi(xi);
        c.setZf(xf);
    }
    
    public int Query(coordenadas c){
        int total = 0;
        int x = 0;
        for (int i = c.getXi(); i <= c.getXf(); i++) {
            for (int j = c.getYi(); j <= c.getYf(); j++) {
                for (int k = c.getZi(); k <= c.getZf(); k++) {
                    total += this.matriz[i][j][k];
                }
            }
        }
        x = newCoordenada();
        c.setXi(x);
        c.setYi(x);
        c.setZi(x);
        System.out.println(total);
        return total;
    }
    
    public int newCoordenada(){
        int coorUpdate = 0;
        for(int i = 0; i<matriz[0].length;i++){
            if(matriz[i][i][i] == 0){
                coorUpdate = i;
                break;
            }
        }
        return coorUpdate;
    }
    
    public void updateSum(int i, int v){
        this.matriz_suma[i] = v;
    }
    
    public int generaCoordenadas(int c_inicial,int c_final){
        return (int)(Math.random()*(c_final-c_inicial)+c_inicial);
    }

}

