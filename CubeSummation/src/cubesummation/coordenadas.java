/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cubesummation;

/**
 *
 * @author juan.santamaria
 */
public class coordenadas {
    private int xi;
    private int yi;
    private int zi;
    private int xf;
    private int yf;
    private int zf;
    private int start;

    public coordenadas(int xi, int yi, int zi, int xf, int yf, int zf) {
        this.xi = xi;
        this.yi = yi;
        this.zi = zi;
        this.xf = xf;
        this.yf = yf;
        this.zf = zf;
        this.start = 0;
    }

    public int getXi() {
        return xi;
    }

    public void setXi(int xi) {
        this.xi = xi;
    }

    public int getYi() {
        return yi;
    }

    public void setYi(int yi) {
        this.yi = yi;
    }

    public int getZi() {
        return zi;
    }

    public void setZi(int zi) {
        this.zi = zi;
    }

    public int getXf() {
        return xf;
    }

    public void setXf(int xf) {
        this.xf = xf;
    }

    public int getYf() {
        return yf;
    }

    public void setYf(int yf) {
        this.yf = yf;
    }

    public int getZf() {
        return zf;
    }

    public void setZf(int zf) {
        this.zf = zf;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }
    
    
}
