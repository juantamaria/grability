/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cubesummation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author juan.santamaria
 */
public class CubeSummation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        StringBuffer sb = new StringBuffer(); 
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ingresar T");
        int T = Integer.parseInt(br.readLine());
        System.out.println("Ingresar N y M");
        String text = br.readLine();
        String[] input = text.split(" "); 
        int N = Integer.parseInt(input[0]); 
        int M = Integer.parseInt(input[1]);
        int W = M -1;
        int start = 1;
        for (int i = 0; i < T; i++) {
            matriz3D m3D = new matriz3D(N,M);
            coordenadas coor = new coordenadas(start,start, start, start, start, start);
            coor.setStart(start);
            int ind_update = 1;
            for (int j = 0; j <= M; j++) {
                if(ind_update%2 != 0){
                    m3D.update(coor, W);
                    W = m3D.generaCoordenadas(0, 1000);
                }else{
                    m3D.updateSum(j, m3D.Query(coor)); 
                }
                ind_update++;
            }
            N = m3D.generaCoordenadas(1, 100);
            M = m3D.generaCoordenadas(1, 1000);
            W = m3D.generaCoordenadas(1, 1000);
            System.out.println(N+" "+M);
        }
    }
    
}
