<?php

	public function post_comfirm(){
		$id_servicio = input::get('servicio_id');
		$id_driver = input::get('driver_id');
		$servicio = Service::find($id_servicio);
		
		if($servicio != NUll){
			if($servicio->status_id == '6'){
				return Response::json(array('error'=>'2'));
			}
			if($servicio->driver_id == NULL && $servicio->status_id == '1'){
				if(!$flag = $this->update($servicio,$id_servicio,$id_driver)){
					return Response::json(array('error'=>'XXXX'));
				}
				$servicio = Service::find($id_servicio);
				$result = $this->userType($servicio);	
			}else{
				return Response::json(array('error'=>'1'));
			}	
			
		}else{
			return Response::json(array('error'=>'3'));
		}
	}

	public function userType($servicio=""){
		$result = "";
		$pushMessage = 'Tu Servicio ha sido confirmato!';
		$push == Push::make();
		if($servicio->user->type == '1'){
			$result = $push->ios($servicio->user->uuid,$pushMessage,1,'honk.wav','Open',array('serviceId'=>$servicio->id));
		}else{
			$result = $push->android2($servicio->user->uuid,$pushMessage,1,'deefault','Open',array('serviceId'=>$servicio->id));
		}
		return $result;
	}


	public function update($servicio="",$id_servicio="",$id_driver=""){
		try{	
			try{
				$servicio = Service::update($id_servicio,array(
					'driver_id'=>$id_driver,
					'status'=>'2'
				));
			}catch(\Illuminate\Database\QueryException $e){
				return false;
			}	
			Driver::update($id_driver,array('available'=>'0'));
			$driverTmp = Driver::find($id_driver);
			Service::update($id_servicio,array(
				'car_id'=>$driverTmp->car_id
			));
		}catch(\Illuminate\Database\QueryException $e){
			return false;
		}
		return true;
	}

<?